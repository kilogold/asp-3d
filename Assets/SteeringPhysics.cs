﻿using UnityEngine;
using System.Collections;

public class SteeringPhysics : MonoBehaviour
{
    public Transform target;
    public float propulsionSpeed;
    private Vector3 shipToTarget;
    public Vector3 steeringVector;

    [Range(0.01f, 3.0f)]
    public float turnSpeed = 1.0f;

    public bool avoidance = false;

    public float MAX_SEE_AHEAD = 25.0f;
    public float MAX_AVOID_FORCE = 1.0f;
    public Vector3 ahead;
    public Vector3 avoidanceForce;

    // Update is called once per frame
    void Update()
    {
        // Determine vector to target (destination)
        shipToTarget = (target.position - transform.position);

        //Get rotational steering vector
        steeringVector = Vector3.RotateTowards(
            transform.forward,
            shipToTarget.normalized,
            turnSpeed * Time.deltaTime,
            0);
        /////////////////////////////////////////////
        if (avoidance)
        {
            ahead = transform.position + transform.forward * MAX_SEE_AHEAD;
            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.forward, out hit, MAX_SEE_AHEAD))
            {
                //Don't know how to scale maxRadiansDelta, but algorithm works as long as turnSpeed == 1.
                steeringVector = Vector3.RotateTowards(
                    steeringVector,
                    hit.normal,
                    turnSpeed * 2f * Time.deltaTime, 
                    0);
            }
        }

        /////////////////////////////////////////////
        transform.rotation = Quaternion.LookRotation(steeringVector);

        transform.position += steeringVector * propulsionSpeed * Time.deltaTime;
    }

    public bool drawGizmo = false;
    void OnDrawGizmos()
    {
        if (drawGizmo)
        {
            //Gizmos.color = Color.red;
            //Gizmos.DrawLine(Vector3.zero, avoidanceForce * 250f);

            //Gizmos.color = Color.yellow;
            //Gizmos.DrawLine(transform.position, transform.position + shipToTarget * 250.0f);

            Gizmos.color = Color.cyan;
            Gizmos.DrawLine(transform.position, transform.position + transform.forward * 250.0f);
        }
    }
}