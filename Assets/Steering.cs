﻿using UnityEngine;
using System.Collections;

//WorkInProgress
public class Steering : MonoBehaviour
{
    public Transform target;
    public float roationSpeed;
    public float propulsionSpeed;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        float steerInfluenceVectorY = 0;
        float steerInfluenceVectorX = 0;

        {
            //Top-Down View
            Vector3 shipToTarget3DNorm = (target.position - transform.position).normalized;
            Vector2 shipToTarget2DNorm = new Vector2(shipToTarget3DNorm.x, shipToTarget3DNorm.z);
            Vector2 shipForward = new Vector2(transform.right.x, transform.right.z);
            float steeringDotProd = Vector2.Dot(shipForward, shipToTarget2DNorm);

            if (steeringDotProd < 0) //obtuse
            {
                steerInfluenceVectorY = -1;
            }
            else if (steeringDotProd > 0) //acute
            {
                steerInfluenceVectorY = 1;
            }
            else
            {
                Debug.LogError("Steering miscalculation");
                return;
            }
        }
        {
            //Right-Side View
            Vector3 targetToShip3DNorm = (target.position - transform.position).normalized;
            Vector2 targetToShip2DNorm = new Vector2(targetToShip3DNorm.y, targetToShip3DNorm.z);
            Vector2 shipForward = new Vector2(transform.up.y, transform.up.z);
            float steeringDotProd = Vector2.Dot(shipForward, targetToShip2DNorm);


            if (steeringDotProd < 0) //obtuse
            {
                steerInfluenceVectorX = 1;
            }
            else if (steeringDotProd > 0) //acute
            {
                steerInfluenceVectorX = -1;
            }
            else
            {
                Debug.LogError("Steering miscalculation");
                return;
            }
        }

        steerInfluenceVectorX *= roationSpeed * Time.deltaTime;
        steerInfluenceVectorY *= roationSpeed * Time.deltaTime;

        if(steerInfluenceVectorX > steerInfluenceVectorY )
        {
            transform.Rotate( steerInfluenceVectorX * roationSpeed * Time.deltaTime,  0,  0 );
        }
        else
        {
            transform.Rotate(0,steerInfluenceVectorY * roationSpeed * Time.deltaTime, 0);
        }


        transform.position += transform.forward * propulsionSpeed * Time.deltaTime;
    }
}
